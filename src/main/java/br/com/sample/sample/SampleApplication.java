package br.com.sample.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleApplication.class, args);
	}
	
	public void coveredByUnitTest() {
		System.out.println("coveredByUnitTest");
	}

	public void notCovered() {
		System.out.println("notCovered");
	}

}
